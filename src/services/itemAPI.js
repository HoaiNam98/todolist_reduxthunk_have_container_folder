import axios from 'axios'
import { url } from '../constants/apiURL'
var Arr = [
  { id: 1, name: 'Boi' },
  { id: 2, name: 'Ve' },
  { id: 3, name: 'Da banh' }
]

export const viewItemAPI = async () => {
  return Arr
}

export const apiFunction = (endpoint, method, data) => {
  return axios({
    method: method,
    url: `${url}/${endpoint}`,
    data: data
  }).catch(err => {
    console.log(err)
  })
}
