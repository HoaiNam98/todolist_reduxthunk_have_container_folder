import React from "react";
import Header from "../components/header";
import Footer from "../components/footer";
import { Container } from "react-bootstrap";



interface LayoutProps {
  children: JSX.Element[] | JSX.Element;
  auth?: boolean;
  className?: string;
}

const Layout: React.FC<LayoutProps> = ({ children }): JSX.Element => {

  return (
    <section
    >
      <Header
      />
      <Container fluid style={{ padding: 0, margin: 0 }}>
        {children}
      </Container>
      <Footer />
    </section>
  );
};
export default Layout;
