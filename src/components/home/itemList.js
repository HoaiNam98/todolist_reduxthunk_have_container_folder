import React from 'react'
import Items from './items'
// import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { filterItemByName,filterItemByStt } from '../../redux/actions/itemAction'
const Index = ({ items }) => {
  const dispatch = useDispatch()
  const itemObj = items.map((a, i) => {
    return <Items key={i} data={a} />
  })
  return (
    <div>
      <table className='table table-bordered table-hover'>
        <thead>
          <tr>
            <th className='text-center'>STT</th>
            <th className='text-center'>Tên</th>
            <th className='text-center'>Trạng Thái</th>
            <th className='text-center'>Hành Động</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td></td>
            <td>
              <input
                name='filterName'
                type='text'
                className='form-control'
                onChange={e => dispatch(filterItemByName(e.target.value))}
              />
            </td>
            <td>
              <select
                name='filterStt'
                className='form-control'
                  onChange={e => dispatch(filterItemByStt(e.target.value))}
              >
                <option value=''>Tất Cả</option>
                <option value={true}>Kích Hoạt</option>
                <option value={false}>Ẩn</option>
              </select>
            </td>
            <td></td>
          </tr>
          {itemObj}
        </tbody>
      </table>
    </div>
  )
}

export default Index
