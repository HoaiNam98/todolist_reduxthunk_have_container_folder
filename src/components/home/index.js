import React from 'react'
import { Button } from 'react-bootstrap'
import AddForm from './addItemForm'
import ItemList from './itemList'
const Index = ({ items, opening, updItem, onClickBtn,addItemAction, updItemAction }) => {
  React.useEffect(() => {
    console.log(items)
  }, [items])
  return (
    <div>
      <div className='container'>
        <h1>Quản lý công việc</h1>
        <Button
          onClick={() => onClickBtn()}
          border='1px solid yellow'
          className='danger'
          children= {updItem !== null ? 'Cập nhật công việc' : 'Thêm công việc'}
        >
        </Button>
        <hr />
        <div className='row'>
          <div className={opening || updItem !== null ? 'col-lg-4' : 'd-none'}>
            <AddForm updItem={updItem} updItemAction={(val) => updItemAction(val)} addItemAction = {(val) => addItemAction(val)}/>
          </div>
          <div className={opening || updItem !== null ? 'col-lg-8' : 'col-lg-12'}>
            <ItemList items={items} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Index
