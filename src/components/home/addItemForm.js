import React, { useState, useEffect } from 'react'
// import {useDispatch} from 'react-redux'
// import {add_product} from '../redux/actions/index.js'
const Index = ({ addItemAction, updItemAction, updItem }) => {
  const [taskItem, setTaskItem] = useState({
    name: '',
    status: true
  })

  useEffect(() => {
    if (updItem !== null) {
      setTaskItem(updItem)
    }
  }, [updItem])

  useEffect(() => {
    console.log(taskItem)
  }, [taskItem])
  const onSubmit = event => {
    event.preventDefault()
    !taskItem.id ? addItemAction(taskItem) : updItemAction(taskItem)
    setTaskItem({
      name: '',
      status: true
    })
  }

  return (
    <div className='panel panel-warning'>
      <div className='panel-heading'>
        <h3 className='panel-title'>
          {!taskItem.id ? 'Thêm công việc' : 'Cập nhật công việc'}
        </h3>
      </div>
      <div className='panel-body'>
        <form>
          <div className='form-group'>
            <label>Tên :</label>
            <input
              value={taskItem.name}
              type='text'
              className='form-control'
              onChange={event =>
                setTaskItem({ ...taskItem, name: event.target.value })
              }
            />
          </div>
          <label>Trạng Thái :</label>
          <select
            value={taskItem.status}
            className='form-control'
            required='required'
            onChange={event =>
              setTaskItem({ ...taskItem, status: event.target.value })
            }
          >
            <option value={true}>Kích Hoạt</option>
            <option value={false}>Ẩn</option>
          </select>
          <br />
          <div className='text-center'>
            <button
              type='button'
              className='btn btn-warning'
              onClick={e => onSubmit(e)}
              children = {taskItem.id ? 'Cập nhật': 'Thêm'}
            >
            </button>
            &nbsp;
            <button className='btn btn-danger'>Hủy Bỏ</button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default Index
