import {OPEN_FORM} from '../../constants/openForm';

const initialState = {
    opening: false
}

export default (state = initialState, action) =>{
    switch (action.type) {
        case OPEN_FORM:
            return {...state,opening: !state.opening};
        default:
            return {...state};
    }
}