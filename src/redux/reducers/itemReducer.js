import {
  VIEW_ITEM,
  VIEW_ITEM_SUCCESS,
  VIEW_ITEM_ERROR,
  ADD_ITEM_SUCCESS,
  DEL_ITEM_SUCCESS,
  DEL_ITEM_ERROR,
  GET_ITEM_SUCCESS,
  GET_ITEM_ERROR,
  UPD_ITEM,
  UPD_ITEM_SUCCESS,
  UPD_ITEM_ERROR,
  FLT_ITEM_BY_NAME,
  FLT_ITEM_BY_STT
} from '../../constants/items'
import { delUpdItem, beforeAddItem } from '../../containers/common'
const initialState = {
  itemArr: [],
  message: null,
  updItem: null,
  filterBy: {
    name: '',
    status: ''
  }
}
export default (state = initialState, action) => {
  switch (action.type) {
    case VIEW_ITEM_SUCCESS:
      return { ...state, itemArr: action.payload }
    case VIEW_ITEM_ERROR:
      return { ...state, message: action.payload }
    case ADD_ITEM_SUCCESS:
      return {
        ...state,
        itemArr: [...state.itemArr, action.payload]
      }
    case DEL_ITEM_SUCCESS:
      return { ...state, itemArr: delUpdItem(state.itemArr, action.payload) }
    case GET_ITEM_SUCCESS:
      return { ...state, updItem: action.payload }
    case GET_ITEM_ERROR:
      return { ...state, message: action.payload }
    case UPD_ITEM_SUCCESS:
      return {
        ...state,
        itemArr: delUpdItem(state.itemArr, action.payload),
        updItem: null
      }
    case UPD_ITEM_ERROR:
      return { ...state, message: action.payload }
    case FLT_ITEM_BY_NAME:
      return { ...state, filterBy: { ...state.filterBy, name: action.payload } }
    case FLT_ITEM_BY_STT:
      return { ...state, filterBy: { ...state.filterBy, status: action.payload } }
    default:
      return { ...state }
  }
}
