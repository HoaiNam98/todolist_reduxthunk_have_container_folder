  
import { combineReducers } from "redux";
import itemReducer from '../reducers/itemReducer';
import openForm from '../reducers/openForm';
export default combineReducers({
itemReducer, openForm
})
