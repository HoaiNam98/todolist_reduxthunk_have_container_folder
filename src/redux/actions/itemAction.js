import {
  VIEW_ITEM,
  VIEW_ITEM_SUCCESS,
  VIEW_ITEM_ERROR,
  ADD_ITEM,
  ADD_ITEM_SUCCESS,
  ADD_ITEM_ERROR,
  DEL_ITEM_SUCCESS,
  DEL_ITEM_ERROR,
  GET_ITEM,
  GET_ITEM_SUCCESS,
  GET_ITEM_ERROR,
  UPD_ITEM,
  UPD_ITEM_SUCCESS,
  UPD_ITEM_ERROR,
  FLT_ITEM_BY_NAME,
  FLT_ITEM_BY_STT


} from '../../constants/items'
import { apiFunction } from '../../services/itemAPI'
export const viewItem = () => {
  return dispatch => {
    apiFunction('items', 'GET', null)
      .then(res => dispatch(viewItemSuccess(res.data)))
      .catch(err => dispatch(viewItemError(err)))
  }
}

const viewItemSuccess = data => {
  return {
    type: VIEW_ITEM_SUCCESS,
    payload: data
  }
}

const viewItemError = err => {
  return {
    type: VIEW_ITEM_ERROR,
    payload: err
  }
}

export const addItem = data => {
  return dispatch => {
    apiFunction('items', 'POST', data)
      .then(res => dispatch(addItemSuccess(res.data)))
      .catch(err => dispatch(addItemError(err)))
  }
}

const addItemSuccess = data => {
  console.log(data);
  return {
    type: ADD_ITEM_SUCCESS,
    payload: data
  }
}

const addItemError = err => {
  return {
    type: ADD_ITEM_SUCCESS,
    payload: err
  }
}

export const delItem = id => {
  return dispatch => {
    apiFunction(`items/${id}`, 'DELETE', null)
      .then(res => {
        dispatch(delItemSuccess(res.data.id))
        console.log(res)
      })
      .catch(err => dispatch(delItemError(err)))
  }
}

const delItemSuccess = data => {
  return {
    type: DEL_ITEM_SUCCESS,
    payload: data
  }
}

const delItemError = err => {
  return {
    type: DEL_ITEM_ERROR,
    payload: err
  }
}

export const getItem = id => {
  return dispatch => {
    apiFunction(`items/${id}`, 'GET')
      .then(res => dispatch(getItemSuccess(res.data)))
      .catch(err => dispatch(getItemError(err)))
  }
}

const getItemSuccess = data => {
  return {
    type: GET_ITEM_SUCCESS,
    payload: data
  }
}

const getItemError = err => {
  return {
    type: GET_ITEM_ERROR,
    payload: err
  }
}

export const updItemm = updData => {
  return dispatch => {
    apiFunction(`items/${updData.id}`, 'PUT', updData)
      .then(res =>dispatch(updItemSuccess(res.data)))
      .catch(err => dispatch(updItemError(err)))
  }
}

const updItemSuccess = (data) =>{
return {
  type: UPD_ITEM_SUCCESS,
  payload: data
}
}

const updItemError = (err) =>{
return {
  type: UPD_ITEM_ERROR,
  payload: err
}
}

export const filterItemByName = (val) =>{
return {
  type: FLT_ITEM_BY_NAME, 
  payload: val
}
}

export const filterItemByStt = (val) =>{
return {
  type: FLT_ITEM_BY_STT, 
  payload: val
}
}