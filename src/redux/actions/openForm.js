import { OPEN_FORM } from '../../constants/openForm'

export const openForm = () => {
  return { type: OPEN_FORM }
}
