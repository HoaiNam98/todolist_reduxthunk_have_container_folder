import React, { lazy, Suspense } from "react";
const HomePage = lazy(() => import("../containers/home"));
const AboutPage = lazy(() => import("../containers/about"));

interface IRoutes {
  match: any;
  history: any;
}
export const NavbarMenu = [
  {
    id: 1,
    path: "/",
    label: "Home",
    activeOnlyWhenExact: true,
    children: [],
    private: false,
    main: () => (
      <Suspense fallback={<p>loading</p>}>
        <HomePage />
      </Suspense>
    ),
  },
  {
    id: 2,
    path: "/about",
    label: "About",
    activeOnlyWhenExact: true,
    children: [],
    private: false,
    main: () => (
      <Suspense fallback={<p>loading</p>}>
        <AboutPage />
      </Suspense>
    ),
  }
];