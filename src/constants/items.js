export const VIEW_ITEM = 'VIEW_ITEM'
export const VIEW_ITEM_SUCCESS = 'VIEW_ITEM_SUCCESS'
export const VIEW_ITEM_ERROR = 'VIEW_ITEM_ERROR'

export const ADD_ITEM = 'ADD_ITEM'
export const ADD_ITEM_SUCCESS = 'ADD_ITEM_SUCCESS'
export const ADD_ITEM_ERROR = 'ADD_ITEM_ERROR'

export const DEL_ITEM = 'DEL_ITEM'
export const DEL_ITEM_SUCCESS = 'DEL_ITEM_SUCCESS'
export const DEL_ITEM_ERROR = 'DEL_ITEM_ERROR'

export const GET_ITEM = 'GET_ITEM'
export const GET_ITEM_SUCCESS = 'GET_ITEM_SUCCESS'
export const GET_ITEM_ERROR = 'GET_ITEM_ERROR'

export const UPD_ITEM = 'UPD_ITEM'
export const UPD_ITEM_SUCCESS = 'UPD_ITEM_SUCCESS'
export const UPD_ITEM_ERROR = 'UPD_ITEM_ERROR'

export const FLT_ITEM_BY_NAME = 'FLT_ITEM_BY_NAME'
export const FLT_ITEM_BY_STT = 'FLT_ITEM_BY_STT'