export const delUpdItem = (Arr, item) => {
  let newArr = Arr
  if (typeof item === 'string') {
    newArr = Arr.filter(i => Number(i.id) !== Number(item))
  } else {
    newArr = Arr.map((i, index) => {
      if (i.id === item.id) i = item
      return i
    })
  }
  return newArr
}

export const beforeAddItem = item => {
  console.log(item.status)
  item.status === 'false' ? (item.status = false) : (item.status = true)
  console.log(typeof item.status)
  return item
}

export const filterFunc = (Arr, val) => {
  let filterArr = Arr.filter(i => {
    if (val.name !== '' && val.status === '')
      return i.name.toLowerCase().includes(val.name.toLowerCase())
    if (val.name === '' && val.status !== '') {
      let formatStt = val.status
      console.log(formatStt, 'before')
      val.status === 'false' ? (formatStt = false) : (formatStt = true)
      console.log(formatStt, 'after')
      return i.status === formatStt
    }
    if (val.name !== '' && val.status !== '') {
      let formatStt = val.status
      val.status === 'false' ? (formatStt = false) : (formatStt = true)
      return (
        i.name.toLowerCase().includes(val.name.toLowerCase()) &&
        i.status === formatStt
      )
    }
    return i
  })
  return filterArr
}

export const findIndex = (Arr, id) => {
  let result = -1
  Arr.forEach((i, index) => {
    if (i.id === id) result = index
  })
  return result
}
