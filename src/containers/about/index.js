import React,{useEffect} from 'react'
import AboutComponent from '../../components/about'
import { useDispatch, useSelector } from 'react-redux'
const Index = () => {
  const { itemArr } = useSelector(state => state.itemReducer)
  useEffect(() => {
    console.log(itemArr, 'hello123')
  }, [itemArr])
  return <AboutComponent />
}
export default Index
