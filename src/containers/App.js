import React from 'react'
import Layout from '../components/index'
import { Provider } from 'react-redux'
import configStore from '../redux/index'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import {NavbarMenu} from '../constants/routes'
import 'bootstrap/dist/css/bootstrap.min.css';
const store = configStore()

const mapRouteMenu = (val: any) => {
  var rs = null
  if (val.length > 0) {
    rs = val.map((item: any, index: number) => {
      // return !item.private ? (
      //   <Route
      //     key={index}
      //     path={item.path}
      //     exact={item.activeOnlyWhenExact}
      //     component={item.main}
      //   />
      // ) : (
      //   <PrivateRoute
      //     key={index}
      //     path={item.path}
      //     exact={item.activeOnlyWhenExact}
      //     component={item.main}
      //   />
      // );
      return (
        <Route
          key={index}
          path={item.path}
          exact={item.activeOnlyWhenExact}
          component={item.main}
        />
      )
    })
  }
  return rs
}
const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <Layout className='App'>
          <Switch>{mapRouteMenu(NavbarMenu)}</Switch>
        </Layout>
      </Router>
    </Provider>
  )
}

export default App
