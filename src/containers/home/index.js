import React, { useEffect } from 'react'
import HomeComponent from '../../components/home'
import { useDispatch, useSelector } from 'react-redux'
import { viewItem, addItem, updItemm } from '../../redux/actions/itemAction'
import { openForm } from '../../redux/actions/openForm'
import { beforeAddItem, filterFunc, findIndex } from '../common'
const Index = () => {
  const dispatch = useDispatch()
  const { itemArr, updItem, filterBy } = useSelector(state => state.itemReducer)
  const { opening } = useSelector(state => state.openForm)
  useEffect(() => {
    dispatch(viewItem())
  }, [dispatch])
  return (
    <HomeComponent
      addItemAction={val => dispatch(addItem(beforeAddItem(val)))}
      updItemAction={val => dispatch(updItemm(beforeAddItem(val)))}
      items={filterBy.name === '' && filterBy.status === '' ? itemArr : filterFunc(itemArr, filterBy)}
      updItem={updItem}
      opening={opening}
      onClickChangeStt = {(val) => console.log(val)}
      onClickBtn={() => dispatch(openForm())}
    />
  )
}

export default Index
